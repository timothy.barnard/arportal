//
//  ViewController.swift
//  ARPortal
//
//  Created by Timothy on 12/10/2018.
//  Copyright © 2018 Timothy. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController {

    @IBOutlet var sceneView: ARSCNView!
    var foundPlane: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set the view's delegate
        sceneView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let touchLocation = touch.location(in: sceneView)
            let results = sceneView.hitTest(touchLocation, types: .existingPlaneUsingExtent)
            if let hitResult = results.first {
                if let boxScene = SCNScene(named: "art.scnassets/portal.scn") {
                    if let box = boxScene.rootNode.childNode(withName: "portal", recursively: true) {
                        
                        box.position = SCNVector3(
                            hitResult.worldTransform.columns.3.x,
                            hitResult.worldTransform.columns.3.y + 0.05,
                            hitResult.worldTransform.columns.3.z
                            )
                        sceneView.scene.rootNode.addChildNode(box)
                        
                        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(rec:)))
                        //Add recognizer to sceneview
                        sceneView.addGestureRecognizer(tap)
                    }
                }
            }
        }
    }
    
    //Method called when tap
    @objc func handleTap(rec: UITapGestureRecognizer){
        
        if rec.state == .ended {
            let location: CGPoint = rec.location(in: sceneView)
            let hits = self.sceneView.hitTest(location, options: nil)
            if !hits.isEmpty, let tappedNode = hits.first?.node {
                if let nodeName = tappedNode.name, nodeName == "tvScreen" {
                    
                    let spriteKitScene = SKScene(size: CGSize(width: 500, height: 500))
                    spriteKitScene.scaleMode = .aspectFit
                    
                    // Create a video player, which will be responsible for the playback of the video material
                    let videoUrl = Bundle.main.url(forResource: "video", withExtension: "mp4")!
                    let videoPlayer = AVPlayer(url: videoUrl)
                    
                    // Create the SpriteKit video node, containing the video player
                    let videoSpriteKitNode = SKVideoNode(avPlayer: videoPlayer)
                    videoSpriteKitNode.position = CGPoint(x: spriteKitScene.size.width / 2.0, y: spriteKitScene.size.height / 2.0)
                    videoSpriteKitNode.size = spriteKitScene.size
                    videoSpriteKitNode.yScale = -1.0
                    videoSpriteKitNode.play()
                    spriteKitScene.addChild(videoSpriteKitNode)
                    
                    tappedNode.geometry?.firstMaterial?.diffuse.contents = spriteKitScene
                    tappedNode.geometry?.firstMaterial?.isDoubleSided = true
                    
                    tappedNode.geometry?.firstMaterial?.diffuse.contentsTransform = SCNMatrix4Translate(SCNMatrix4MakeScale(-1, 1, 1), 1, 0, 0)

                }
            }
        }
    }
}

// MARK: - ARSCNViewDelegate
extension ViewController: ARSCNViewDelegate {
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        if !foundPlane, let planeAnchor = anchor as? ARPlaneAnchor {
            let plane = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z))
            let planeNode = SCNNode()
            planeNode.position = SCNVector3(planeAnchor.center.x, 0, planeAnchor.center.z)
            planeNode.transform = SCNMatrix4MakeRotation(-Float.pi/2, 1, 0, 0)
            
            let gridMaterial = SCNMaterial()
            gridMaterial.diffuse.contents = UIImage(named: "art.scnassets/grid.png")
            plane.materials = [gridMaterial]
            
            planeNode.geometry = plane
            node.addChildNode(planeNode)
            foundPlane = true
        } else {
            return
        }
    }
    
    /*
     // Override to create and configure nodes for anchors added to the view's session.
     func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
     let node = SCNNode()
     
     return node
     }
     */
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}
